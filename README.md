# Random Password Generator

Generates a random password with those rules : 
* Minimum length : 8 
* Two characters of the same type (uppercase, lowercase, symbol, digits) are not consecutive.
*  One letter does not appear twice (case insensitive). 

## Authors
**Hakim ABDOUROIHAMANE** - [alsagone](https://gitlab.com/alsagone)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites
Having Java installed on your PC.

## Launching
```
javac RandomPassword.java && java RandomPassword <length>
```

## Built With
[Visual Studio Code](https://code.visualstudio.com/)