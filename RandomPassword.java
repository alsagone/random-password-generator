import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class RandomPassword
{
    private static final String symbols = "!\"#$%&'()*+,-./.:;<=>?@[\\]^_`{|}~" ;
    private static final int upperCaseLetter = 0 ; 
    private static final int lowerCaseLetter = 1 ; 
    private static final int symbol = 2 ; 
    private static final int digit = 3 ;
    
    //returns a random number (min <= randomNumber <= max)
    public static int myRand (int a, int b)
    {
        int min = Math.min(a,b) ;
        int max = Math.max(a,b) ; 
        Random r = new Random() ;
        return r.nextInt(max-min+1) + min ; 
    }

    //returns the type of the character
    public static int type (char c)
    {
        int t = (-1) ;

        if (Character.isUpperCase(c)) {t = upperCaseLetter ;}
        else if (Character.isLowerCase(c)) {t = lowerCaseLetter ;}
        else if (symbols.indexOf(c) != (-1)) {t = symbol ;}
        else if (Character.isDigit(c)) {t = digit ;}
        else 
        {
            System.err.println("Unknown character : " + c) ;
            System.exit(1) ;
        }

        return t ;
    }

    //returns a random symbol 
    public static char randomSymbol()
    {
        int randomIndex = myRand(0, symbols.length()-1) ;
        return symbols.charAt(randomIndex) ;
    }

    //return a random character from an other type
    public static char randomChar (int excludeType)
    {
        int choice = excludeType ;
        char c = ' ' ; 

        while (choice == excludeType)
        {
            choice = myRand(upperCaseLetter, digit) ;
        }

        switch (choice)
        {
            case upperCaseLetter : c = (char) myRand('A', 'Z') ; break ;
            case lowerCaseLetter : c = (char) myRand('a', 'z') ; break ;
            case symbol : c = randomSymbol() ; break ;
            case digit : c = (char) myRand('0', '9') ; break ;
            default : break ;
        }

        return c ;
    }

    //returns true if the char is in the password (case insensitive)
    public static boolean myContains (char c, String password)
    {
        String passwordLowerCase = password.toLowerCase() ;
        char cLowerCase = Character.toLowerCase(c) ;
        return (passwordLowerCase.indexOf(cLowerCase) != (-1)) ;
    }

    public static String buildPassword (int length)
    { 
        //Minimum length = 8
        if (length < 8)
        {
            System.out.println("Most of password policies require a minimum length of 8 chars.") ;
            length = 8 ; 
        }

        //The first char doesn't have a specific type so we use the parameter (-1)
        char c = randomChar(-1) ;

        StringBuilder password = new StringBuilder() ;
        password.append(c) ;

        char lastChar ;
        int lastCharType ; 

        while (password.length() < length) 
        {
            lastChar = password.charAt(password.length()-1) ;
            lastCharType = type(lastChar) ;

            while (myContains(c,password.toString()))
            {
                c = randomChar(lastCharType) ;
            }

            password.append(c) ;
        }

        return password.toString() ;
    }

    public static void getRandomPassword (int length)
    {
        String password = buildPassword(length) ;
        System.out.println(password + "\n") ;
        return ;
    }
    public static void main(String[] args) throws IOException
    {
        int length ; 

        if (args.length > 0)
        {
            for (String s : args)
            {
                length = Integer.parseInt(s) ;
                getRandomPassword(length) ;
            }
        }

        else 
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Desired length(s) : ") ;
            String input = br.readLine() ;
            String[] lengthsArray = input.trim().split("\\s+");

            for (String s : lengthsArray)
            {
                length = Integer.parseInt(s) ;
                getRandomPassword(length) ;
            }
        }

        return ;
    }

    
}